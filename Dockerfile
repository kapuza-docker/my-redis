FROM my-ubuntu:latest
MAINTAINER Kaka Puzikova <gitlab@tost.info.tm>

RUN apt -y install \
    redis-server \
    redis-tools

RUN sed -i 's/^bind.*$/bind 0.0.0.0/' /etc/redis/redis.conf

# Add volumes for MySQL 
VOLUME  ["/etc/redis", "/var/lib/redis"]

EXPOSE 6379

CMD /etc/init.d/redis-server restart && tail -f /var/log/redis/redis-server.log
