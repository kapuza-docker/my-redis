# Docker / my-redis
Простой имидж redis+ubuntu.
## Получаем репу
```bash
git clone git@gitlab.com:kapuza-docker/my-redis.git
cd my-redis
git config user.name "Kaka Puzikova"; git config user.email "gitlab@tost.info.tm"
```
## Использование
```bash
# Сборка имиджа (если он уже склонирован)
docker build -t my-redis .

# Можно прям из репы (если лень клонировать)
docker build -t my-git git@gitlab.com:kapuza-docker/my-redis.git

# Создаем контейнер
docker run -d -P --name redis my-redis

# Создаем еще один контейнер, чтоб подключиться к этому
docker run -it --link redis:redis my-ubuntu /bin/bash
apt install redis-tools
redis-cli -h redis
> ping
> set foo 100
> incr foo
> get foo
> exit
```
